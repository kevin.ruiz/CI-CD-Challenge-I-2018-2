'use strict';

const express = require('express');

// Constants
const PORT = 8000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
    function control(petic, resp) {
        var date = new Date();
        var month = date.getMonth()+1;
        var day = date.getDate();
        var year = date.getFullYear();
        var hour = date.getHours();
        var min = date.getMinutes();
        var sec = date.getSeconds();
        resp.writeHead(200, {'content-type': 'text/html'});
        const time =  day.toString() + "/" + month.toString() + "/" + year.toString() +  "  " + hour.toString() + ":" + min.toString() + ":" + sec.toString()
        resp.write(`<h1>Hello Kevin, how are u?</h1><p>${time}</p>`);
        resp.end();
    }
    control(req,res)
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
